#include "pch.h"

#ifndef DRIVERLISTENERTHREAD_H
#define DRIVERLISTENERTHREAD_H

#include "commportclient.h"
#include "logdllmodule.h"
#include "log.h"

class DriverListenerThread final
{
private:
    std::atomic_bool m_started;
    CommPortClient* m_commPortClient;
    LogDllModule* m_logDllModule;

private:
    bool parseDriverLogToJson(const Log& log, std::string& jsonMessage);

public:
    DriverListenerThread();

    void close();
    void run();
};

#endif // DRIVERLISTENERTHREAD_H