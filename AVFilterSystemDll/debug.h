#include "pch.h"

#ifndef DEBUG_H
#define DEBUG_H

namespace Debug
{
void log(const std::string&);
void log(const std::wstring&);
}

#endif // DEBUG_H