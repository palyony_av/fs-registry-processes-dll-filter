#include "pch.h"

#ifndef LOG_H
#define LOG_H

class Log
{
public:
    unsigned int id;

    std::string event;
    std::string date;
    std::string time;
   
    Log() 
        : id(0)
    {
    }

    Log(int id_, std::string event_, std::string date_, std::string time_)
        : id(id_),
        event(event_),
        date(date_),
        time(time_)
    {
    }
};


#endif // LOG_H