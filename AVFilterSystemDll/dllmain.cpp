#include "pch.h"

#include "debug.h"

BOOL APIENTRY DllMain(HMODULE hModule, DWORD ul_reason_for_call, LPVOID lpReserved)
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        Debug::log("DLL has been attached to process");
        break;
    case DLL_PROCESS_DETACH:
        Debug::log("DLL has been datached from process");
        break;
    }

    return TRUE;
}
