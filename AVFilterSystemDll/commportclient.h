#include "pch.h"

#ifndef COMMPORTCLIENT_H
#define COMMPORTCLIENT_H

#include "log.h"

class CommPortClient
{
private:
    HANDLE m_hPort;

    CommPortClient();
    ~CommPortClient();

    CommPortClient(CommPortClient const&) = delete;
    CommPortClient& operator= (CommPortClient const&) = delete;

    void connectToDriver();
    void disconnectFromDriver();

public:
    static CommPortClient* getInstance(bool needReconnect = false);

    bool sendMessage(const std::string& message) const;
    bool getMessage(Log &log) const;
};

#endif // COMMPORTCLIENT_H