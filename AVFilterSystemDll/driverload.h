#ifndef DRIVERLOAD_H
#define DRIVERLOAD_H

bool loadDriver();
bool unloadDriver();

#endif // DRIVERLOAD_H