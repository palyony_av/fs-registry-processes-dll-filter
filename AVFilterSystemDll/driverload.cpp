#include "pch.h"

#include "driverload.h"
#include "debug.h"

#define SERVICE_NAME "FsFilterDriver"
#define INF_FILE     "driver\\FsFilterDriver.inf"

SC_HANDLE hSCManager = NULL;
SC_HANDLE hService = NULL;

bool loadDriver()
{
    SERVICE_STATUS_PROCESS ssp;
    DWORD dwBytesNeeded;
    DWORD dwStartTime = GetTickCount();
    DWORD dwTimeout = 5000; // 5-second time-out

    /*
     * If ShellExecute succeeds, it returns a value greater than 32.
     * If the function fails, it returns an error value that indicates
     * the cause of the failure
     */

    const unsigned long ERROR_BOUNDARY = 32;
    HINSTANCE hInstance = ShellExecuteA(NULL, "install", INF_FILE, NULL, NULL, SW_SHOWNORMAL);

    if ((unsigned long)hInstance <= ERROR_BOUNDARY)
    {
        Debug::log("FsFilterDriver installation failed");
        return false;
    }

    // =========================================================================
    // Start driver service using SCManager
    // =========================================================================

    hSCManager = OpenSCManagerA(NULL, NULL, SC_MANAGER_ALL_ACCESS);

    if (!hSCManager)
    {
        Debug::log("OpenSCManager failed: " + std::to_string(GetLastError()));
        return false;
    }

    const int ATTEMPTS_NUMBER = 5;

    // =========================================================================
    // Try ATTEMPTS_NUMBER attempts to open the driver service
    // =========================================================================

    for (int i = 0; i < ATTEMPTS_NUMBER; ++i)
    {
        std::this_thread::sleep_for(std::chrono::seconds(1));

        hService = OpenServiceA(hSCManager, SERVICE_NAME, SERVICE_QUERY_STATUS | SERVICE_START | SERVICE_STOP);

        if (hService)
            break;
    }

    if (!hService)
    {
        Debug::log("OpenService failed: " + std::to_string(GetLastError()));
        CloseServiceHandle(hSCManager);
        hSCManager = NULL;
        return false;
    }

    // =========================================================================
    // Make sure the service is not already running
    // =========================================================================

    BOOL result = QueryServiceStatusEx(hService, SC_STATUS_PROCESS_INFO, (LPBYTE)& ssp, sizeof(SERVICE_STATUS_PROCESS), &dwBytesNeeded);

    if (!result)
    {
        Debug::log("QueryServiceStatusEx failed: " + std::to_string(GetLastError()));
        CloseServiceHandle(hService);
        CloseServiceHandle(hSCManager);
        hSCManager = hService = NULL;
        return FALSE;
    }

    if (ssp.dwCurrentState == SERVICE_RUNNING)
        return true;

    // =========================================================================
    // Attempt to start the service
    // =========================================================================

    result = StartServiceA(hService, 0, NULL);

    if (!result)
    {
        Debug::log("StartService failed: " + std::to_string(GetLastError()));
        CloseServiceHandle(hService);
        CloseServiceHandle(hSCManager);
        hSCManager = hService = NULL;
        return false;
    }

    // =========================================================================
    // Wait for the service to start
    // =========================================================================

    while (ssp.dwCurrentState != SERVICE_RUNNING)
    {
        Sleep(ssp.dwWaitHint);

        result = QueryServiceStatusEx(hService, SC_STATUS_PROCESS_INFO, (LPBYTE)& ssp, sizeof(SERVICE_STATUS_PROCESS), &dwBytesNeeded);

        if (!result)
        {
            Debug::log("QueryServiceStatusEx failed: " + std::to_string(GetLastError()));
            break;
        }

        if (ssp.dwCurrentState == SERVICE_RUNNING)
            break;

        if (GetTickCount() - dwStartTime > dwTimeout)
        {
            Debug::log("Wait timed out");
            break;
        }
    }

    return true;
}

bool unloadDriver()
{
    BOOL result;
    SERVICE_STATUS_PROCESS ssp;
    DWORD dwBytesNeeded;
    DWORD dwStartTime = GetTickCount();
    DWORD dwTimeout = 5000; // 5-second time-out

    if (hService)
    {
        result = QueryServiceStatusEx(hService, SC_STATUS_PROCESS_INFO, (LPBYTE)& ssp, sizeof(SERVICE_STATUS_PROCESS), &dwBytesNeeded);

        if (!result)
        {
            Debug::log("QueryServiceStatusEx failed: " + std::to_string(GetLastError()));
            return FALSE;
        }

        if (ssp.dwCurrentState == SERVICE_STOPPED)
            goto exit;

        // =========================================================================
        // Send a stop code to the service
        // =========================================================================

        result = ControlService(hService, SERVICE_CONTROL_STOP, (LPSERVICE_STATUS)& ssp);

        if (!result)
        {
            Debug::log("ControlService failed: " + std::to_string(GetLastError()));
            return FALSE;
        }

        // =========================================================================
        // Wait for the service to stop
        // =========================================================================

        while (ssp.dwCurrentState != SERVICE_STOPPED)
        {
            Sleep(ssp.dwWaitHint);

            result = QueryServiceStatusEx(hService, SC_STATUS_PROCESS_INFO, (LPBYTE)& ssp, sizeof(SERVICE_STATUS_PROCESS), &dwBytesNeeded);

            if (!result)
            {
                Debug::log("QueryServiceStatusEx failed: " + std::to_string(GetLastError()));
                break;
            }

            if (ssp.dwCurrentState == SERVICE_STOPPED)
                break;

            if (GetTickCount() - dwStartTime > dwTimeout)
            {
                Debug::log("Wait timed out");
                goto exit;
            }
        }
    }

exit:
    if (hService)
    {
        CloseServiceHandle(hService);
        hService = NULL;
    }

    if (hSCManager)
    {
        CloseServiceHandle(hSCManager);
        hSCManager = NULL;
    }

    return TRUE;
}