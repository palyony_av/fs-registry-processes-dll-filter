#include "pch.h"

#include "driverlistenerthread.h"
#include "debug.h"

#include "jsoncpp/json/json.h"

#define MODULE_ID 1

DriverListenerThread::DriverListenerThread()
    : m_started(true)
{
    try
    {
        m_commPortClient = CommPortClient::getInstance();
    }
    catch (const std::exception& /*e*/)
    {
        throw;
    }

    m_logDllModule = LogDllModule::getInstance();

    if (m_logDllModule == nullptr)
    {
        const std::string errorMessage("failed to load dll module");
        Debug::log(errorMessage);
        throw std::exception(errorMessage.c_str());
    }
}

void DriverListenerThread::close()
{
    m_started = false;
}

void DriverListenerThread::run()
{
    bool result;

    Log log;
    std::string messageToGui;

    while (m_started)
    {
        result = m_commPortClient->getMessage(log);

        if (result && !log.event.empty())
        {
            messageToGui.clear();

            parseDriverLogToJson(log, messageToGui);

            if (m_logDllModule)
                m_logDllModule->fInsertJsonEntry(messageToGui.c_str(), static_cast<uint32_t>(messageToGui.size()));
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(200));
    }
}

bool DriverListenerThread::parseDriverLogToJson(const Log& log, std::string& jsonMessage)
{
    Json::Value root;

    root["id"]        = 0;
    root["module_id"] = MODULE_ID;
    root["event"]     = log.event;
    root["date"]      = log.date;
    root["time"]      = log.time;

    Json::StreamWriterBuilder builder;
    jsonMessage = Json::writeString(builder, root);

    return true;
}