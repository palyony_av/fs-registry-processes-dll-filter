#ifndef LOGMODULECONTEXT_H
#define LOGMODULECONTEXT_H

#include <windows.h>
#include <string>
#include <memory>

/* Store path for all dll modules here */
#define DLL_LOG_MODULE_NAME "Logger.dll"
#define DLL_LOG_MODULE_PATH "lib/" DLL_LOG_MODULE_NAME

#define FUNC_INSERT_JSON_ENTRY "insertJsonEntry"

#define AV_NOT_ENOUGH_MEMORY 2
#define AV_ERRONEOUS_INPUT 1
#define AV_SUCCESS 0

#define DEFAULT_LOG_SIZE 512

typedef void (*func_insertJsonEntry)(const char *, uint32_t);

class LogDllModule
{
public:
    static LogDllModule* getInstance();
    func_insertJsonEntry fInsertJsonEntry;

    // TODO: Add threads to checks logs

private:
    LogDllModule(HMODULE hOpenedLogLib);
    ~LogDllModule();
    LogDllModule(const LogDllModule&) = delete;
    LogDllModule& operator=(const LogDllModule&) = delete;

    HMODULE hDllModule;
};

#endif // LOGMODULECONTEXT_H
