#include "pch.h"
#include "logdllmodule.h"

LogDllModule* LogDllModule::getInstance()
{
    static HMODULE hDllModule = nullptr;

    if (hDllModule == nullptr)
    {
        hDllModule = LoadLibraryA(DLL_LOG_MODULE_PATH);
        if (hDllModule == nullptr)
        {
            return nullptr;
        }

        if (!GetProcAddress(hDllModule, FUNC_INSERT_JSON_ENTRY))
        {
            FreeLibrary(hDllModule);
            hDllModule = nullptr;
            return nullptr;
        }
    }

    static LogDllModule logDllModule(hDllModule);
    return &logDllModule;
}

LogDllModule::LogDllModule(HMODULE hOpenedLogLib)
    : hDllModule(hOpenedLogLib)
{
    this->fInsertJsonEntry = reinterpret_cast<func_insertJsonEntry>(
        GetProcAddress(this->hDllModule, FUNC_INSERT_JSON_ENTRY)
        );
}

LogDllModule::~LogDllModule()
{
    FreeLibrary(this->hDllModule);
}
