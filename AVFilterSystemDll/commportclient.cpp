#include "pch.h"

#include "commportclient.h"

#include "debug.h"

#pragma comment(lib, "user32.lib")
#pragma comment(lib, "kernel32.lib")
#pragma comment(lib, "fltlib.lib")

#define COMM_PORT_NAME L"\\PAV_CommPortFilterSubsystem"

#define BUFFER_LENGTH 1024

typedef struct _DRIVER_NOTIFICATION
{
    wchar_t EventBuffer[BUFFER_LENGTH];
    int     EventLength;
    wchar_t DateBuffer[BUFFER_LENGTH];
    wchar_t TimeBuffer[BUFFER_LENGTH];
} DRIVER_NOTIFICATION, PDRIVER_NOTIFICATION;

struct DRIVER_MESSAGE : public FILTER_MESSAGE_HEADER, public DRIVER_NOTIFICATION
{};

CommPortClient::CommPortClient()
    : m_hPort(nullptr)
{
    try
    {
        connectToDriver();
    }
    catch (const std::exception /*e*/)
    {
        throw;
    }
}

CommPortClient::~CommPortClient()
{
    disconnectFromDriver();
}

CommPortClient* CommPortClient::getInstance(bool needReconnect)
{
    try
    {
        static CommPortClient commPortClient;

        if (needReconnect)
        {
            commPortClient.disconnectFromDriver();
            commPortClient.connectToDriver();
        }

        return &commPortClient;
    }
    catch (const std::exception& /*e*/)
    {
        throw;
    }
}

bool CommPortClient::sendMessage(const std::string& message) const
{
    HRESULT res;
    DWORD dwBytesReturned;

    res = FilterSendMessage(
        m_hPort, 
        (LPVOID)message.c_str(), 
        static_cast<DWORD>(message.size()), 
        NULL, 
        NULL, 
        &dwBytesReturned);

    return res == S_OK;
}

bool CommPortClient::getMessage(Log& log) const
{
    HRESULT res;
    DRIVER_MESSAGE driverMessage;
    std::wstring wevent;
    std::wstring wdate;
    std::wstring wtime;

    res = FilterGetMessage(m_hPort, &driverMessage, sizeof(DRIVER_MESSAGE), nullptr);
  
    if (res == S_OK)
    {
        if (driverMessage.EventLength != 0)
        {
            Debug::log("Message has been received from FsFilterDriver:");
            Debug::log(driverMessage.EventBuffer);

            wevent = driverMessage.EventBuffer;
            wdate = driverMessage.DateBuffer;
            wtime = driverMessage.TimeBuffer;

            using convert_type = std::codecvt_utf8<wchar_t>;
            std::wstring_convert<convert_type, wchar_t> converter;

            log.event = converter.to_bytes(wevent);
            log.date = converter.to_bytes(wdate);
            log.time = converter.to_bytes(wtime);
        }
    }
    else if (res != HRESULT_FROM_WIN32(ERROR_IO_PENDING))
    {
        std::stringstream stream;
        stream << std::hex << res;
        Debug::log("FilterGetMessage has failed: 0x" + stream.str());
    }

    return res == S_OK;
}

void CommPortClient::connectToDriver()
{
    HRESULT res;

    res = FilterConnectCommunicationPort(COMM_PORT_NAME, 0, NULL, 0, NULL, &m_hPort);

    if (res != S_OK)
    {
        const std::string errorMessage(
            std::string("FilterConnectCommunicationPort failed: ") +
            std::to_string(res)
        );

        throw std::exception(errorMessage.c_str());
    }
}

void CommPortClient::disconnectFromDriver()
{
    CloseHandle(m_hPort);
}