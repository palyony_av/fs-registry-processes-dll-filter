#include "pch.h"

#ifndef REGISTRY_H
#define REGISTRY_H

struct ProtectedObject
{
    std::string id;
    std::string path;
    std::string type;
};

class Registry final
{
private:
    HKEY m_hFilterKey;

    Registry();
    ~Registry();

    Registry(Registry const&) = delete;
    Registry& operator= (Registry const&) = delete;

public:
    static Registry* getInstance();

    std::shared_ptr<std::vector<ProtectedObject>> getObjectsToProtect();
};

#endif // REGISTRY_H
