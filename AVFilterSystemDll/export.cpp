#include "pch.h"

#include "export.h"
#include "debug.h"
#include "registry.h"
#include "commportclient.h"
#include "driverlistenerthread.h"
#include "driverload.h"

namespace
{
std::unique_ptr<std::thread> listenerThread;
std::unique_ptr<DriverListenerThread> listener;
bool isFirstStart = true;

void formMessage(std::shared_ptr<std::vector<ProtectedObject>> objects, std::string& message)
{
    for (auto it = objects->cbegin(); it != objects->cend(); it++)
    {
        /*
         * take the first letter from entry type
         * example: Directory -> D, File -> F, Registry key -> R
         * message line example: f_user/desktop/some.txt\n
         */

        message += it->type.substr(0, 1) + "_" + it->path + "\n";
    }
}
} // namespace

unsigned int updateModuleData(AVHMODULE /*module*/)
{
    Registry* reg = nullptr;
    CommPortClient* commPortClient = nullptr;
    std::shared_ptr<std::vector<ProtectedObject>> objects;

    try
    {
        reg = Registry::getInstance();
        objects = reg->getObjectsToProtect();

        commPortClient = CommPortClient::getInstance();
    }
    catch(const std::exception& e)
    {
        Debug::log("updateProtectedObjects failed: " + std::string(e.what()));
        return AV_STATUS_FAIL;
    }

    // transform json data to another format 
    std::string messageToDriver;
    formMessage(objects, messageToDriver);

    Debug::log("Message to driver: " + messageToDriver);

    // send data to driver via communication port
    commPortClient->sendMessage(messageToDriver);

    return AV_STATUS_SUCCESS;
}

AVHMODULE installModule()
{
    if (!loadDriver())
        return nullptr;

    try
    {
        listener = std::make_unique<DriverListenerThread>();

        if (isFirstStart)
        {
            isFirstStart = false;
        }
        else
        {
            // make reconnect to driver
            CommPortClient::getInstance(true);
        }
    }
    catch (const std::exception& e)
    {
        Debug::log(e.what());
        return nullptr;
    }

    listenerThread = std::make_unique<std::thread>([](){ listener->run(); });

    return reinterpret_cast<AVHMODULE>(1);
}

unsigned int uninstallModule(AVHMODULE /*module*/)
{
    Debug::log("uninstallModule function call");

    bool result;

    result = unloadDriver();

    if (!result)
        return AV_STATUS_FAIL;

    listener->close();

    if (listenerThread->joinable())
        listenerThread->join();

    Debug::log("listener thread has finished");

    return AV_STATUS_SUCCESS;
}