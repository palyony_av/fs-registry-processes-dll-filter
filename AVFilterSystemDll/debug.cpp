#include "pch.h"

#include "debug.h"

#define DEBUG_FILE_NAME "Debug_FilterSubsystem.txt"

void Debug::log(const std::string& message)
{
    std::ofstream debugFile(DEBUG_FILE_NAME, std::ios_base::app);
    debugFile.write(message.c_str(), message.length());
    debugFile.write("\n", 1);
    debugFile.close();
}

void Debug::log(const std::wstring& wmessage)
{
    std::string message(wmessage.begin(), wmessage.end());
    Debug::log(message);
}