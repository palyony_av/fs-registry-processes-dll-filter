#include "pch.h"

#include "registry.h"

#define AV_FILTER_REG_KEY "SOFTWARE\\PalyonyAV\\FilterSubsystem"

#define MAX_VALUE_NAME 1024

Registry::Registry()
    : m_hFilterKey(nullptr)
{
    DWORD retCode;

    retCode = RegCreateKeyExA(HKEY_LOCAL_MACHINE, AV_FILTER_REG_KEY, 0, NULL, 0, KEY_ALL_ACCESS, NULL, &this->m_hFilterKey, NULL);

    if (retCode != ERROR_SUCCESS)
    {
        std::string errorMessage = std::string("Registry::RegCreateKeyEx failed: ") + std::to_string(retCode);
        throw std::runtime_error(errorMessage);
    }
}

Registry::~Registry()
{
    if (this->m_hFilterKey)
        RegCloseKey(this->m_hFilterKey);
}

Registry* Registry::getInstance()
{
    try
    {
        static Registry reg;
        return &reg;
    }
    catch (const std::exception& /*e*/)
    {
        throw;
    }
}

std::shared_ptr<std::vector<ProtectedObject>> Registry::getObjectsToProtect()
{
    auto objects = std::make_shared<std::vector<ProtectedObject>>();

    DWORD nValues;
    DWORD i, retCode;

    CHAR achValue[MAX_VALUE_NAME];
    DWORD cchValue = MAX_VALUE_NAME;

    retCode = RegQueryInfoKey(this->m_hFilterKey, NULL, NULL, NULL, NULL, NULL, NULL, &nValues, NULL, NULL, NULL, NULL);

    if (retCode == ERROR_SUCCESS && nValues > 0)
    {
        for (i = 0, retCode = ERROR_SUCCESS; i < nValues; i++)
        {
            cchValue = MAX_VALUE_NAME;
            achValue[0] = '\0';
            retCode = RegEnumValueA(this->m_hFilterKey, i, achValue, &cchValue, NULL, NULL, NULL, NULL);

            if (retCode == ERROR_SUCCESS)
            {
                char valueBuffer[MAX_VALUE_NAME];
                DWORD bufferSize = sizeof(valueBuffer);

                retCode = RegGetValueA(HKEY_LOCAL_MACHINE, AV_FILTER_REG_KEY, achValue, RRF_RT_ANY, NULL, &valueBuffer, &bufferSize);

                if (retCode == ERROR_SUCCESS)
                {
                    Json::Value root;
                    Json::CharReaderBuilder builder;
                    const std::unique_ptr<Json::CharReader> reader(builder.newCharReader());
                    JSONCPP_STRING err;

                    if (reader->parse(valueBuffer, valueBuffer + bufferSize, &root, &err))
                    {
                        ProtectedObject object;

                        object.id = achValue;
                        object.type = root["type"].asString();
                        object.path = root["path"].asString();

                        objects->push_back(object);
                    }
                    else
                    {
                        std::string errorMessage{"failed to parse json string"};
                        throw std::runtime_error(errorMessage);
                    }
                }
            }
        }
    }

    return objects;
}