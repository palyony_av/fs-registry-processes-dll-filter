#include "pch.h"

#ifndef EXPORT_H
#define EXPORT_H

#define AV_STATUS_SUCCESS 1
#define AV_STATUS_FAIL 0

using AVHMODULE = void*;

/* Notify driver that protected objects are updated */
extern "C" __declspec(dllexport) unsigned int updateModuleData(AVHMODULE);

/* Installation preparations for module work */
extern "C" __declspec(dllexport) AVHMODULE installModule();

/* Release all module resources */
extern "C" __declspec(dllexport) unsigned int uninstallModule(AVHMODULE);

#endif // EXPORT_H